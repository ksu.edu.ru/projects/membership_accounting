package com.example.membership_accounting.logic;

import com.example.membership_accounting.logic.models.Client;
import com.example.membership_accounting.logic.models.Subscription;
import com.example.membership_accounting.logic.models.Visit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public abstract class DB {
    /**Список клиентов*/
    public HashMap<Integer, Client> Clients = new HashMap<Integer, Client>();

    /**Список всех абонементов клиента*/
    public HashMap<Integer, Subscription> Subscriptions = new HashMap<Integer, Subscription>();

    /**Список посещенных тренировок*/
    public HashMap<Integer, Visit> Visits = new HashMap<Integer, Visit>();

    public int generateIdToClient(){
        return Clients.size() == 0? 0: Collections.max(Clients.keySet()) + 1;
    }

    public int generateIdToSubscription(){
        return Subscriptions.size() == 0? 0: Collections.max(Subscriptions.keySet()) + 1;
    }

    public int generateIdToVisit(){
        return Visits.size() == 0? 0: Collections.max(Visits.keySet()) + 1;
    }

    public abstract void Load();
    public abstract void Save();
}
