package com.example.membership_accounting.logic.models;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Objects;

/**Абонемент*/
public class Subscription {
    /**Дата/время регистрации клиента*/
    public LocalDate CreatedAt;
    /**Цена абонемента*/
    public int Price;
    /**Количество тренировок*/
    public int TrainingCount;
    /**Список посещенных тренировок*/
    public LinkedHashMap<Integer, Visit> Visits = new LinkedHashMap<>();

    public LocalDateTime getLastTrainingDateTime(){
        return Objects.requireNonNull(Visits.get(Collections.max(Visits.keySet()))).CreatedAt;
    }

    /**Метод добавления тренировки*/
    public boolean addVisit(int id, Visit visit) {
        if (TrainingCount == 0)
            return false;

        TrainingCount--;
        Visits.put(id, visit);
        return true;
    }

    public int visitCount(){
        return Visits.size();
    }

    public void removeVisit(int visitId){
        if(Visits.remove(visitId) != null){
            TrainingCount++;
        };
    }
}
