package com.example.membership_accounting.logic;

import android.annotation.SuppressLint;
import android.media.Image;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.barcode.BarcodeScanner;
import com.google.mlkit.vision.barcode.BarcodeScannerOptions;
import com.google.mlkit.vision.barcode.BarcodeScanning;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.common.InputImage;

import java.util.List;

public class BarCodeAnalyzer implements ImageAnalysis.Analyzer{

    OnSuccessListener<List<Barcode>> onSuccessListener;
    OnFailureListener onFailureListener;
    public BarCodeAnalyzer(OnSuccessListener<List<Barcode>> onSuccessListener, OnFailureListener onFailureListener){
        this.onSuccessListener = onSuccessListener;
        this.onFailureListener = onFailureListener;
    }

    @Override
    public void analyze(@NonNull ImageProxy image) {
        scanBarcode(image);
    }

    private void scanBarcode(ImageProxy image) {
        @SuppressLint("UnsafeOptInUsageError") Image image1 = image.getImage();
        assert image1 != null;
        InputImage inputImage = InputImage.fromMediaImage(image1, image.getImageInfo().getRotationDegrees());
        BarcodeScannerOptions options =
                new BarcodeScannerOptions.Builder()
                        .setBarcodeFormats(
                                Barcode.FORMAT_CODE_128)
                        .build();
        BarcodeScanner scanner = BarcodeScanning.getClient(options);


        Task<List<Barcode>> result = scanner.process(inputImage)
                .addOnSuccessListener(barcodes -> {
                    onSuccessListener.onSuccess(barcodes);
                    image.close();
                })
                .addOnFailureListener(e -> {
                    onFailureListener.onFailure(e);
                    image.close();
                });
    }
}
