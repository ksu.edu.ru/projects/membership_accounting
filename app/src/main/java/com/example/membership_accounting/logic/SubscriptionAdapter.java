package com.example.membership_accounting.logic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.membership_accounting.R;
import com.example.membership_accounting.logic.models.Subscription;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.SubscriptionViewHolder> {

    LinkedHashMap<Integer, Subscription> subscriptions;
    ArrayList<Pair<Integer, Subscription>> listSubscriptions;
    final OnSubscriptionClickListener onClickListener;

    public SubscriptionAdapter(LinkedHashMap<Integer, Subscription> subscriptions, OnSubscriptionClickListener onClickListener){
        this.subscriptions = subscriptions;
        this.onClickListener = onClickListener;
    }

    public interface OnSubscriptionClickListener{
        void OnSubscriptionClick(Context context, int id);
    }

    @NonNull
    @Override
    public SubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int subItem = R.layout.activity_subscription_item;

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(subItem, parent, false);

        return new SubscriptionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionViewHolder holder, @SuppressLint("RecyclerView") int position) {
        listSubscriptions = new ArrayList<>(subscriptions.size());
        subscriptions.forEach((key, value) -> listSubscriptions.add(new Pair<>(key, value)));
        Collections.reverse(listSubscriptions);

        holder.bind(position);

        holder.itemView.setOnClickListener(view -> {
            int id = listSubscriptions.get(position).first;
            onClickListener.OnSubscriptionClick(view.getContext(), id);
        });
    }

    @Override
    public int getItemCount() {
        return subscriptions.size();
    }

    class SubscriptionViewHolder extends RecyclerView.ViewHolder{

        TextView subscriptionInfo;

        public SubscriptionViewHolder(@NonNull View itemView) {
            super(itemView);

            subscriptionInfo = itemView.findViewById(R.id.subscription_info);
        }

        void bind(int index){
            Subscription subscription = listSubscriptions.get(index).second;
            String subInfo = subscription.CreatedAt.format(DateTimeFormatter.ofPattern(itemView.getContext().getString(R.string.date_format)))
                    + " - " + subscription.visitCount() + " / " + (subscription.TrainingCount + subscription.visitCount());
            subscriptionInfo.setText(subInfo);
        }
    }

}
