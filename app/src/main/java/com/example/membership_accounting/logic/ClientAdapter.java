package com.example.membership_accounting.logic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.membership_accounting.R;
import com.example.membership_accounting.logic.models.Client;

import java.util.ArrayList;
import java.util.HashMap;

public class ClientAdapter extends RecyclerView.Adapter<ClientAdapter.ClientViewHolder> {

    private final OnClientClickListener onClickListener;
    HashMap<Integer, Client> clients;
    ArrayList<Pair<Integer, Client>> listClients;

    public ClientAdapter(HashMap<Integer, Client> clients, OnClientClickListener onClickListener){
        this.clients = clients;
        this.onClickListener = onClickListener;
    }

    public interface OnClientClickListener{
        void onClientClick(int position, Context context);
    }

    @NonNull
    @Override
    public ClientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int idClientItem = R.layout.activity_client_item;

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(idClientItem, parent, false);

        ClientViewHolder viewHolder = new ClientViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ClientViewHolder holder, @SuppressLint("RecyclerView") int position) {
        listClients = new ArrayList<>(clients.size());
        clients.forEach((key, value) -> listClients.add(new Pair<>(key, value)));

        holder.bind(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = listClients.get(position).first;

                //Вызов метода onClientClick из переменной слушателя и передаем id
                onClickListener.onClientClick(id, view.getContext());
            }
        });
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    class ClientViewHolder extends RecyclerView.ViewHolder{

        TextView clientName;

        public ClientViewHolder(@NonNull View itemView) {
            super(itemView);
            clientName = itemView.findViewById(R.id.clientName_textView);
        }

        void bind(int index){
            Client client = listClients.get(index).second;
            clientName.setText(client.Surname + " " + " " + client.Name);
        }
    }
}
