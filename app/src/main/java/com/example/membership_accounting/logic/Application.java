package com.example.membership_accounting.logic;

import androidx.annotation.NonNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

import com.example.membership_accounting.logic.models.*;

public class Application {
    public DB DB;
    private static Application application;


    private Application(DB db) {
        DB = db;
    }

    public static Application createApplication(DB db) {
        if (application == null)
            application = new Application(db);
        return application;
    }

    public static Application getApplication() {
        if (application != null) {
            return application;
        }
        else {
            throw new NullPointerException();
        }
    }

    public void addClient(@NonNull Client client) {
        client.CreatedAt = LocalDate.now();
        DB.Clients.put(DB.generateIdToClient(), client);
        DB.Save();
    }

    public void addSubscription(@NonNull Client client, @NonNull Subscription subscription) {
        int id = DB.generateIdToSubscription();
        subscription.CreatedAt = LocalDate.now();
        DB.Subscriptions.put(id, subscription);
        client.addSubscription(id, subscription);
        DB.Save();
    }

    public boolean addVisit(@NonNull Subscription subscription, @NonNull Visit visit) {
        int id = DB.generateIdToVisit();
        visit.CreatedAt = LocalDateTime.now();
        if(subscription.addVisit(id, visit)){
            DB.Visits.put(id, visit);
            DB.Save();
            return true;
        }

        return false;
    }

    public boolean addVisit(@NonNull Client client, Visit visit){
        if(client.getSubscriptionsCount() == 0)
            return  false;

        for(Map.Entry<Integer, Subscription> entry: client.Subscriptions.entrySet()){
            if(entry.getValue().TrainingCount > 0){
                return addVisit(entry.getValue(), visit);
            }
        }

        return false;
    }

    public void removeVisit(int subscriptionId, int visitId) {
        Objects.requireNonNull(DB.Subscriptions.get(subscriptionId)).removeVisit(visitId);
        DB.Visits.remove(visitId);
        DB.Save();
    }
}
