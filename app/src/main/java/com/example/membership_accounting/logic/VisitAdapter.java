package com.example.membership_accounting.logic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.membership_accounting.R;
import com.example.membership_accounting.logic.models.Subscription;
import com.example.membership_accounting.logic.models.Visit;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class VisitAdapter extends RecyclerView.Adapter<VisitAdapter.VisitViewHolder> {

    LinkedHashMap<Integer, Visit> visits;
    ArrayList<Pair<Integer, Visit>> listVisits;

    int subId;
    final OnVisitClickListener onVisitClickListener;
    public interface OnVisitClickListener{
        void onDeleteClick(int subscriptionId, int visitId, int position);
    }

    public VisitAdapter(LinkedHashMap<Integer, Visit> visits, OnVisitClickListener onVisitClickListener, int subId){
        this.visits = visits;
        this.onVisitClickListener = onVisitClickListener;
        this.subId = subId;
    }

    class VisitViewHolder extends RecyclerView.ViewHolder {
        TextView visitInfo;
        ImageButton deleteBtn;

        @SuppressLint("NotifyDataSetChanged")
        public VisitViewHolder(@NonNull View itemView) {
            super(itemView);
            visitInfo = itemView.findViewById(R.id.visit_info);
            deleteBtn = itemView.findViewById(R.id.delete_visit_btn);
        }

        public void bind(int index){
            Visit visit = listVisits.get(index).second;
            visitInfo.setText(visit.CreatedAt.format(DateTimeFormatter.ofPattern(itemView.getContext().getString(R.string.date_time_format))));
        }
    }

    @NonNull
    @Override
    public VisitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int subItem = R.layout.activity_visit_item;

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(subItem, parent, false);

        return new VisitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitViewHolder holder, int position) {
        listVisits = new ArrayList<>(visits.size());
        visits.forEach((key, value) -> listVisits.add(new Pair<>(key, value)));
        Collections.reverse(listVisits);
        holder.bind(position);

        holder.deleteBtn.setOnClickListener(view -> {
            int visitId = listVisits.get(position).first;
            onVisitClickListener.onDeleteClick(subId, visitId, position);
            Log.d("demo", "click " + position + " delete " + visitId);
        });
    }

    @Override
    public int getItemCount() {
        return visits.size();
    }
}
