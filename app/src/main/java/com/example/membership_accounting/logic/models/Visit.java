package com.example.membership_accounting.logic.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**Тренировка*/
public class Visit {
    /**Абонемент, по которому регистрируется тренировка*/
    Subscription Subscription;
    /**Дата/время регистрирации тренировки*/
    public LocalDateTime CreatedAt;
}
