package com.example.membership_accounting.logic;

import androidx.annotation.NonNull;

import com.example.membership_accounting.logic.models.Client;
import com.example.membership_accounting.logic.models.Subscription;
import com.example.membership_accounting.logic.models.Visit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;


public class JSONContext extends DB {

    private final String path;

    public JSONContext(String path) {
        this.path = path;

        Load();
    }

    public void Load(){
        String jsonString = "";
        try {
            FileReader fr = new FileReader(path);
            BufferedReader reader = new BufferedReader(fr);
            jsonString = reader.readLine();

            if(jsonString.length() == 0){
                Save();
                return;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            getVisitsFromJSON(jsonObject.getJSONArray("Visits"));
            getSubscriptionsFromJSON(jsonObject.getJSONArray("Subscriptions"));
            getClientsFromJSON(jsonObject.getJSONArray("Clients"));


        } catch (JSONException e) {
            Save();
            e.printStackTrace();
        }

    }

    public void Save() {
        JSONObject jsonObject = new JSONObject();

        JSONArray jsonSubscriptions = getJSONFromSubscriptions();
        JSONArray jsonClients = getJSONFromClients();
        JSONArray jsonVisits = getJSONFromVisits();

        try {
            jsonObject.put("Clients", jsonClients);
            jsonObject.put("Subscriptions", jsonSubscriptions);
            jsonObject.put("Visits", jsonVisits);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try (FileWriter file = new FileWriter(path)) {
            file.write(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  JSONArray getJSONFromClients(){
        JSONArray clients = new JSONArray();
        Clients.forEach((id, client) -> {
            try {
            JSONObject jsonClient = new JSONObject();
            jsonClient.put("Id", id);
            jsonClient.put("CreatedAt", client.CreatedAt.toString());
            jsonClient.put("Surname", client.Surname);
            jsonClient.put("Name", client.Name);
            jsonClient.put("Patronymic", client.Patronymic);
            jsonClient.put("Phone", client.Phone);

            JSONArray subscriptions = new JSONArray();
            for(int subId: client.Subscriptions.keySet()){
                subscriptions.put(subId);
            }
            jsonClient.put("Subscriptions", subscriptions);

            clients.put(jsonClient);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        return clients;
    }

    public void getClientsFromJSON(@NonNull JSONArray clients){
        for(int i = 0; i < clients.length(); i++){
            try {
                JSONObject jsonClient = clients.getJSONObject(i);
                Client client = new Client();

                client.Surname = jsonClient.getString("Surname");
                client.Name = jsonClient.getString("Name");
                client.Patronymic = jsonClient.getString("Patronymic");
                client.Phone = jsonClient.getString("Phone");
                client.CreatedAt = LocalDate.parse(jsonClient.getString("CreatedAt"));

                JSONArray subscriptions =  jsonClient.getJSONArray("Subscriptions");

                for(int j = 0; j < subscriptions.length(); j++){
                    int id = subscriptions.getInt(j);
                    client.addSubscription(id, Subscriptions.get(id));
                }

                Clients.put(jsonClient.getInt("Id"), client);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private JSONArray getJSONFromSubscriptions() {
        JSONArray subscriptions = new JSONArray();
        Subscriptions.forEach((id, subscription) -> {
            JSONObject jsonSubscription = new JSONObject();
            try {
                jsonSubscription.put("Id", id);
                jsonSubscription.put("CreatedAt",subscription.CreatedAt.toString());
                jsonSubscription.put("Price", String.valueOf(subscription.Price));
                jsonSubscription.put("TrainingCount", String.valueOf(subscription.TrainingCount));

                JSONArray visits = new JSONArray();
                for(int subId: subscription.Visits.keySet()){
                    visits.put(subId);
                }
                jsonSubscription.put("Visits", visits);

                subscriptions.put(jsonSubscription);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        return subscriptions;
    }


    public void getSubscriptionsFromJSON(@NonNull JSONArray subscriptions){
        for(int i = 0; i < subscriptions.length(); i++){
            try {
                JSONObject jsonSubscription = subscriptions.getJSONObject(i);
                Subscription subscription = new Subscription();

                subscription.Price = jsonSubscription.getInt("Price");
                subscription.TrainingCount = jsonSubscription.getInt("TrainingCount");
                subscription.CreatedAt = LocalDate.parse(jsonSubscription.getString("CreatedAt"));
                JSONArray visits =  jsonSubscription.getJSONArray("Visits");

                for(int j = 0; j < visits.length(); j++){
                    int id = visits.getInt(j);
                    subscription.Visits.put(id, Visits.get(id));
                }

                Subscriptions.put(jsonSubscription.getInt("Id"), subscription);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public JSONArray getJSONFromVisits() {
        JSONArray visits = new JSONArray();

        Visits.forEach((id, visit) -> {
            JSONObject jsonVisit = new JSONObject();
            try {
                jsonVisit.put("Id", id);
                jsonVisit.put("CreatedAt", visit.CreatedAt.toString());

                visits.put(jsonVisit);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        return visits;
    }

    public void getVisitsFromJSON(@NonNull JSONArray visits){
        for(int i = 0; i < visits.length(); i++){
            try {
                JSONObject jsonVisit = visits.getJSONObject(i);
                Visit visit = new Visit();
                visit.CreatedAt = LocalDateTime.parse(jsonVisit.getString("CreatedAt"));
                Visits.put(jsonVisit.getInt("Id"), visit);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
