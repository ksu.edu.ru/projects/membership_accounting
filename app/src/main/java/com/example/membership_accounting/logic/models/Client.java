package com.example.membership_accounting.logic.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Objects;

/**Клиент*/
public class Client {
    /**Дата/время регистрации*/
    public LocalDate CreatedAt;
    /**Фамилия*/
    public String Surname;
    /**Имя*/
    public String Name;
    /**Отчество*/
    public String Patronymic = "";
    /**Контактный телефон*/
    public String Phone;
    /**Список абонементов клиента*/
    public LinkedHashMap<Integer, Subscription> Subscriptions = new LinkedHashMap<Integer, Subscription>();
    /**Добавление абонемента для клиента*/
    public void addSubscription(int id, Subscription subscription) {
        Subscriptions.put(id, subscription);
    }

    /**Количество абонементов клиента*/
    public int getSubscriptionsCount(){
        return Subscriptions.size();
    }

    public LocalDateTime getLastVisitDateTime(){
        LocalDateTime lastDate = LocalDateTime.MIN;

        for(Subscription sub: Subscriptions.values()){
            if(sub.Visits.size() == 0)
                continue;
            LocalDateTime date = sub.getLastTrainingDateTime();
            if (date.compareTo(lastDate) > 0) {
                lastDate = date;
            }
        }
        return  lastDate;
    }
}
