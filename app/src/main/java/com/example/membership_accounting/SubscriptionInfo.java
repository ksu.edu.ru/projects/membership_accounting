package com.example.membership_accounting;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.membership_accounting.logic.Application;
import com.example.membership_accounting.logic.VisitAdapter;
import com.example.membership_accounting.logic.models.Subscription;
import com.example.membership_accounting.logic.models.Visit;

import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class SubscriptionInfo extends AppCompatActivity {
    int id;
    Subscription subscription;
    Application application;

    TextView subNumber;
    TextView regDate;
    TextView priceText;
    TextView trainingCount;
    RecyclerView visitsList;

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscritpion_info);

        id = getIntent().getExtras().getInt("id");
        application = Application.getApplication();
        subscription = application.DB.Subscriptions.get(id);

        subNumber = findViewById(R.id.subNumber);
        regDate = findViewById(R.id.acquiredDate);
        priceText = findViewById(R.id.price);
        trainingCount = findViewById(R.id.trainingCount);
        visitsList = findViewById(R.id.visitsRecycler);

        findViewById(R.id.add_visit_button).setOnClickListener(view -> {
            if(application.addVisit(subscription, new Visit())){
                UpdateData();
                RecyclerView.Adapter adapter = visitsList.getAdapter();
                adapter.notifyItemInserted(0);
                visitsList.scrollToPosition(0);
                adapter.notifyItemRangeChanged(1,  adapter.getItemCount());
                setResult(1);
            }
            else {
                Toast.makeText(this, getString(R.string.subscription_warning), Toast.LENGTH_LONG).show();
            }
        });

        UpdateData();
        loadVisits();
    }

    @SuppressLint("SetTextI18n")
    void UpdateData(){
        subNumber.setText(String.valueOf(id));
        regDate.setText(subscription.CreatedAt.format(DateTimeFormatter.ofPattern(getString(R.string.date_format))));
        priceText.setText(subscription.Price + "р");
        trainingCount.setText(subscription.visitCount() + " / " + (subscription.TrainingCount + subscription.visitCount()));
    }

    void loadVisits(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        visitsList.setLayoutManager(layoutManager);
        @SuppressLint("NotifyDataSetChanged") VisitAdapter.OnVisitClickListener onVisitClickListener = (subscriptionId, visitId ,position) -> {
            application.removeVisit(subscriptionId, visitId);
            UpdateData();
            setResult(1);
            RecyclerView.Adapter adapter = visitsList.getAdapter();
            adapter.notifyItemRemoved(position);
            adapter.notifyItemRangeChanged(position, adapter.getItemCount());
        };

        visitsList.setAdapter(new VisitAdapter(subscription.Visits, onVisitClickListener, id));
    }
}