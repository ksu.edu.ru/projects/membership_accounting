package com.example.membership_accounting;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.example.membership_accounting.logic.Application;
import com.example.membership_accounting.logic.models.Client;

public class MakeOrEditClient extends AppCompatActivity {

    Context context;
    Application application;
    Client client;
    EditText surnameEdit;
    EditText nameEdit;
    EditText patronymicEdit;
    EditText phoneEdit;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_client);

        context = getApplicationContext();
        application = Application.getApplication();
        int clientId = getIntent().getIntExtra("clientId", -1);

        surnameEdit = findViewById(R.id.surname);
        nameEdit = findViewById(R.id.name);
        patronymicEdit = findViewById(R.id.patronymic);
        phoneEdit = findViewById(R.id.phone);

        if(clientId != -1){
            client = application.DB.Clients.get(clientId);
            assert client != null;
            surnameEdit.setText(client.Surname);
            nameEdit.setText(client.Name);
            patronymicEdit.setText(client.Patronymic);
            phoneEdit.setText(client.Phone);
            ((TextView)findViewById(R.id.textnewClient)).setText(R.string.edit_client);
        }

        findViewById(R.id.createClient).setOnClickListener(view -> {
            String surname = surnameEdit.getText().toString();
            String name = nameEdit.getText().toString();
            String patronymic = patronymicEdit.getText().toString();
            String phone = phoneEdit.getText().toString();

            if(surname.length() == 0 || name.length() == 0 || phone.length() == 0){
                Toast.makeText(this, getResources().getText(R.string.warning).toString(), Toast.LENGTH_LONG).show();
                return;
            }

            if(client == null){
                client = new Client();
                application.addClient(client);
            }

            client.Surname = surname;
            client.Name = name;
            client.Patronymic = patronymic;
            client.Phone = phone;

            application.DB.Save();
            setResult(1);
            finish();
        });
    }
}
