package com.example.membership_accounting;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.membership_accounting.logic.Application;
import com.example.membership_accounting.logic.SubscriptionAdapter;
import com.example.membership_accounting.logic.models.Client;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

import java.io.File;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class ClientInfo extends AppCompatActivity {
    Application application;
    RecyclerView listSubscriptions;
    int id;
    Client client;
    TextView clientName;
    TextView lastTrainingDate;
    TextView phone;
    final String BARCODE_DIR = "/barcodes";

    private boolean isReadPermissionGranted = false;
    private boolean isWritePermissionGranted = false;

    ActivityResultLauncher<Intent> activityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == 1) {
                        Objects.requireNonNull(listSubscriptions.getAdapter()).notifyDataSetChanged();
                    }
                }
            }
    );

    ActivityResultLauncher<Intent> editClientResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == 1) {
                        application.DB.Save();
                        fillFields();

                        setResult(1);
                    }
                }
            }
    );

    ActivityResultLauncher<Intent> addVisitResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == 1) {
                        Objects.requireNonNull(listSubscriptions.getAdapter()).notifyDataSetChanged();
                        lastTrainingDate.setText(client.getLastVisitDateTime().format(DateTimeFormatter.ofPattern(getString(R.string.date_format))));
                    }
                }
            }
    );

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_info);
        application = Application.getApplication();
        Bundle arguments = getIntent().getExtras();
        id = (int) arguments.get("id");
        client = application.DB.Clients.get(id);

        LoadSubscriptions();

        clientName = findViewById(R.id.client_name);

        fillFields();

        lastTrainingDate = findViewById(R.id.lastdate);
        LocalDateTime visitDateTime = client.getLastVisitDateTime();
        lastTrainingDate.setText(visitDateTime.compareTo(LocalDateTime.MIN) == 0 ? "-" :
                visitDateTime.format(DateTimeFormatter.ofPattern(getString(R.string.date_format))));

        findViewById(R.id.edit_client_button).setOnClickListener(view -> {
            Intent intent = new Intent(this, MakeOrEditClient.class);
            intent.putExtra("clientId", id);
            editClientResultLauncher.launch(intent);
        });

        // По клику на кнопку открывается активити NewSubscriptionActivity
        findViewById(R.id.newSubscription_btn).setOnClickListener(view -> {
            Intent intent = new Intent(this, NewSubscriptionActivity.class);
            intent.putExtra("id", id);
            activityLauncher.launch(intent);
        });

        // "Поделиться штрих-кодом"
        findViewById(R.id.qr_btn).setOnClickListener(view -> {
            requestPermission();

            Intent shareBarcode = new Intent(Intent.ACTION_SEND);
            shareBarcode.setType("image/jpg");

            Bitmap barcode = generateBarcode(id);
            Uri barcodeUri = saveImageToExternalStorage("client № " + id + " - " + client.Surname, barcode);

            shareBarcode.putExtra(Intent.EXTRA_STREAM, barcodeUri);
            startActivity(Intent.createChooser(shareBarcode, "Поделиться штрихкодом"));
        });

    }

    protected void fillFields(){
        clientName.setText(client.Surname + " " + client.Name + " " + client.Patronymic);
        phone = findViewById(R.id.phoneText);
        phone.setText(client.Phone);
    }

    protected void LoadSubscriptions() {
        listSubscriptions = findViewById(R.id.subscriptions_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listSubscriptions.setLayoutManager(layoutManager);
        SubscriptionAdapter.OnSubscriptionClickListener onClickListener = (context, id) -> {
            Intent intent = new Intent(context, SubscriptionInfo.class);
            intent.putExtra("id", id);
            addVisitResultLauncher.launch(intent);
        };
        listSubscriptions.setAdapter(new SubscriptionAdapter(client.Subscriptions, onClickListener));
    }

    private Uri saveImageToExternalStorage(String imgName, Bitmap bmp){
        Uri ImageCollection = null;
        ContentResolver resolver = getContentResolver();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            ImageCollection = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY);

        }else {
            ImageCollection = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, imgName + ".jpg");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + BARCODE_DIR);
        }
        contentValues.put(MediaStore.Images.Media.MIME_TYPE,"image/jpeg");
        Uri imageUri = resolver.insert(ImageCollection, contentValues);

        try {
            OutputStream outputStream = resolver.openOutputStream(Objects.requireNonNull(imageUri));
            bmp.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
            Objects.requireNonNull(outputStream);
            return imageUri;
        }
        catch (Exception e){

            Toast.makeText(this,"Штрихкод не сохранен: \n" + e,Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return imageUri;
    }


    Bitmap generateBarcode (int clientId) {
        String content = getString(R.string.barcode_prefix) + clientId;
        float scale = getResources().getDisplayMetrics().density;
        Client client = application.DB.Clients.get(clientId);
        String clientName = client.Surname + " " + client.Name;

        Bitmap image = Bitmap.createBitmap(640, 360, Bitmap.Config.ARGB_8888);
        image.eraseColor(Color.WHITE);
        Canvas canvas = new Canvas(image);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(20 * scale);
        Rect bounds = new Rect();
        paint.getTextBounds(clientName, 0, clientName.length(), bounds);
        int x = (image.getWidth() - bounds.width()) / 2;
        int y = (image.getHeight() - bounds.height());
        canvas.drawText(clientName, x, y, paint);

        BitMatrix bitMatrix = new Code128Writer().encode(content, BarcodeFormat.CODE_128, image.getWidth(),
                image.getHeight() - bounds.height() * 5, null);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();

        Bitmap barcodeBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                barcodeBitmap.setPixel(i, j, bitMatrix.get(i, j) ? Color.BLACK : Color.WHITE);
            }
        }

        paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        canvas.drawBitmap(barcodeBitmap, 0, bounds.height(), paint);

        return image;
    }

    //Лаунчер запроса разрешений
    ActivityResultLauncher<String[]> mPermissionResultLauncher = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {

            if (result.get(Manifest.permission.READ_EXTERNAL_STORAGE) != null){
                isReadPermissionGranted = Boolean.TRUE.equals(result.get(Manifest.permission.READ_EXTERNAL_STORAGE));
            }
            if (result.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) != null){
                isWritePermissionGranted = Boolean.TRUE.equals(result.get(Manifest.permission.WRITE_EXTERNAL_STORAGE));
            }
        }
    });

    //Запрос разрешений на запись и чтение файлов
    private void requestPermission() {


        boolean minSDK = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q;

        isReadPermissionGranted = ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED;

        isWritePermissionGranted = ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED;

        isWritePermissionGranted = isWritePermissionGranted || minSDK;

        List<String> permissionRequest = new ArrayList<String>();

        if (!isReadPermissionGranted){
            permissionRequest.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!isWritePermissionGranted){
            permissionRequest.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!permissionRequest.isEmpty()){
            mPermissionResultLauncher.launch(permissionRequest.toArray(new String[0]));
        }
    }
}

