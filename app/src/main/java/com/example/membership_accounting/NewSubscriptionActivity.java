package com.example.membership_accounting;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.membership_accounting.logic.Application;
import com.example.membership_accounting.logic.models.Client;
import com.example.membership_accounting.logic.models.Subscription;

public class NewSubscriptionActivity extends AppCompatActivity {

    Context context;
    Application application;
    Client client;
    Subscription subscription;
    TextView price;
    TextView trainingCount;
    int id;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_subscription);

            context = getApplicationContext();
            application = Application.getApplication();
            Bundle arguments = getIntent().getExtras();
            id = (int) arguments.get("id");
            client = application.DB.Clients.get(id);

            price = findViewById(R.id.editTextNumber);
            trainingCount = findViewById(R.id.editTextNumber2);

            //По клику на кнопку "Сохранить" добавляется новый абонемент
            findViewById(R.id.saveSubscription_btn).setOnClickListener(view -> {

            String inputPrice = price.getText().toString();
            String inputTrainingCount = trainingCount.getText().toString();
            //Проверка на наличие данных
            if (inputPrice.length() == 0 || inputTrainingCount.length() == 0) {
                Toast.makeText(this, getResources().getText(R.string.warning).toString(), Toast.LENGTH_LONG).show();
                return;
            }

            subscription = new Subscription();
            subscription.Price = Integer.parseInt(inputPrice);
            subscription.TrainingCount = Integer.parseInt(inputTrainingCount);

            application.addSubscription(client, subscription);
            application.DB.Save();
            setResult(1);
            finish();
        });
    }
}