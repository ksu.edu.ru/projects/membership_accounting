package com.example.membership_accounting;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.membership_accounting.logic.Application;
import com.example.membership_accounting.logic.ClientAdapter;
import com.example.membership_accounting.logic.JSONContext;
import com.example.membership_accounting.logic.models.Visit;

import java.util.Objects;

public class ClientsActivity extends AppCompatActivity {

    Application application;
    Context context;

    RecyclerView listClients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients);

        context = getApplicationContext();
        JSONContext jsonContext = new JSONContext(context.getFilesDir().getPath() + "/" + getString(R.string.Json_path));
        application = Application.createApplication(jsonContext);

        LoadClients();


        Button newClient = findViewById(R.id.newClientBtn);
        Button scanner = findViewById(R.id.scanBtn);

        //Сканирование штрих-кода
        scanner.setOnClickListener(view -> {
            Intent intent = new Intent(this, ScannerActivity.class);
            scannerActivityLauncher.launch(intent);
        });

        newClient.setOnClickListener(view -> {
            Intent intent = new Intent(this, MakeOrEditClient.class);
            clientLauncher.launch(intent);
        });
    }

    protected void LoadClients(){
        listClients = findViewById(R.id.clients_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listClients.setLayoutManager(layoutManager);
        ClientAdapter.OnClientClickListener clientClickListener = new ClientAdapter.OnClientClickListener() {
            @Override
            public void onClientClick(int id, Context context) {
                openClientInfo(context, id);
            }
        };
        ClientAdapter clientAdapter = new ClientAdapter(application.DB.Clients, clientClickListener);
        listClients.setAdapter(clientAdapter);
    }

    ActivityResultLauncher<Intent> clientLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == 1){
                        Objects.requireNonNull(listClients.getAdapter()).notifyDataSetChanged();
                    }
                }
            }
    );

    ActivityResultLauncher<Intent> scannerActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
               if(result.getResultCode() == 1){
                   int id = result.getData().getIntExtra("id", -1);
                   if(id > -1){
                       if(!application.addVisit(Objects.requireNonNull(application.DB.Clients.get(id)), new Visit())){
                           Toast.makeText(this, getString(R.string.subscription_warning), Toast.LENGTH_LONG).show();
                       };
                       openClientInfo(this, id);
                   }
               }
            }
    );

    void openClientInfo(Context context, int id) {
        Intent intent = new Intent(context, ClientInfo.class);
        intent.putExtra("id", id);
        clientLauncher.launch(intent);
    }
}